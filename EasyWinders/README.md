Easy Winders
============

![Paracord hanks and winder](images/paracord.jpeg)

I've been on a paracord buying spree and needed a storage solution; here are the sturdy, easy to make cardboard winders I've come up with.

I've been using the first one as a template so I can make them all pretty standard, but you'll see those existing cuts even in the first steps.

Supplies:
- Sharp knife or heavy-duty scissors
- Corrugated cardboard
- Wide packing tape

1. Start with a piece of cardboard twice as wide as the winder and fold like a book; I put any ink or printing on the inside so it won't rub off.
   ![A piece of cardboard](images/1_cardboard.jpeg)
   ![Folded cardboard](images/2_fold.jpeg)
2. Mark and cut angles for the sides; do not cut across the bottom. I roughly measured these with my thumb to make them more-or-less even - this isn't rocket surgery.

   I use a box knife to cut through both layers at the same time so they match up - BE CAREFUL!
   ![Cuts for winder sides](images/3_mark_and_cut.jpeg)
3. Open and fold the extra pieces inside the "book" and tape the long edge tightly closed.

This adds some thickness to the middle of the winder which seems to make the whole thing much stiffer.
   ![Folded winder](images/4_fold_inside.jpeg)
   ![Edge of the winder](images/5_edge.jpeg)
4. For paracord, I tuck the end in between the folded tabs to start the winding; for something like an extension cord or light strings, I wouldn't.
   ![Cord tucked into the winder](images/6_tuck.jpeg)

To make several, I've been using this one as a template.

---
